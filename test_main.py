from main import *
import unittest
import uuid


class TestAPI(unittest.TestCase):

    def test_uuid(self):
        self.assertEqual(is_valid_uuid(str(uuid.uuid4())), True, 'Should be True')

    def test_name(self):
        self.assertEqual(is_valid_name('Max Mustermann'), True, 'Should be True')


if __name__ == '__main__':
    unittest.main()
