import flask
import os
from dotenv import load_dotenv
import psycopg2
from flask import jsonify
from uuid import UUID
import requests
import re
import jwt

load_dotenv()

app = flask.Flask(__name__)
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'test_key'


def get_db_assets_connection():
    conn = psycopg2.connect(host=os.environ.get('POSTGRES_ASSETS_HOST'),
                            database=os.environ.get('POSTGRES_ASSETS_DBNAME'),
                            user=os.environ.get('POSTGRES_ASSETS_USER'),
                            password=os.environ.get('POSTGRES_ASSETS_PASSWORD'))
    cur = conn.cursor()
    return conn, cur


def close_db_connection(conn, cur):
    cur.close()
    conn.close()
    return


def is_valid_uuid(uuid_to_test):
    try:
        UUID(uuid_to_test, version=4)
    except ValueError:
        return False
    return True


def is_valid_name(name):
    if bool(re.fullmatch('[A-Za-z]{2,25}( [A-Za-z]{2,25})?', name)) is True:
        return True
    else:
        return False


def is_valid_address(address):
    if bool(re.fullmatch('[A-Za-z]{2,25}( [A-Za-z]{2,25})?', address)) is True:
        return True
    else:
        return False


def is_valid_room_name(name):
    if bool(re.fullmatch('[0-9]{3}', name)) is True:
        return True
    else:
        return False


def is_valid_date(date):
    if bool(re.fullmatch('[1-9]4}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])', date)) is True:
        return True
    else:
        return False


# check if too many keys in passed object??
def is_building(data):
    try:
        if is_valid_uuid(data['id']) is False:
            return 'invalid input (id is not an UUID)', False
        elif is_valid_name(data['name']) is False:
            return 'invalid input (name is not valid)', False
        elif is_valid_address(data['address']) is False:
            return 'invalid input (address is not valid)', False
        else:
            return 'valid input', True
    except:
        return 'invalid input (not a building)', False


def is_storey(data):
    try:
        if is_valid_uuid(data['id']) is False:
            return 'invalid input (id is not an UUID)', False
        elif is_valid_uuid(data['building_id']) is False:
            return 'invalid input (building_id is not an UUID)', False
        elif is_valid_name(data['name']) is False:
            return 'invalid input (name is not valid)', False
        else:
            return 'valid input', True
    except:
        return 'invalid input (not a storey)', False


def is_room(data):
    try:
        if is_valid_uuid(data['id']) is False:
            return 'invalid input (id is not an UUID)', False
        elif is_valid_uuid(data['storey_id']) is False:
            return 'invalid input (storey_id is not an UUID)', False
        elif is_valid_room_name(data['name']) is False:
            return 'invalid input (name is not valid)', False
        else:
            return 'valid input', True
    except:
        return 'invalid input (not a room)', False


def is_reservation(data):
    try:
        if is_valid_uuid(data['id']) is False:
            return 'invalid input (id is not an UUID)', False
        elif is_valid_date(data['from']) is False:
            return 'invalid input (from is not a date(YYYY-MM-DD))', False
        elif is_valid_date(data['to']) is False:
            return 'invalid input (to is not a date(YYYY-MM-DD))', False
        elif is_valid_uuid(data['room_id']) is False:
            return 'invalid input (room_id ist not an UUID)', False
        else:
            return 'valid input', True
    except:
        return 'invalid input (not a reservation)', False


def check_if_building_exists(uuid):
    conn, cur = get_db_assets_connection()
    try:
        cur.execute('SELECT id FROM buildings WHERE id = %s', (uuid,))
        return True if cur.fetchone() is not None else False
    finally:
        close_db_connection(conn, cur)


def check_if_storey_exists(uuid):
    conn, cur = get_db_assets_connection()
    try:
        cur.execute('SELECT id FROM storeys WHERE id = %s', (uuid,))
        return True if cur.fetchone() is not None else False
    finally:
        close_db_connection(conn, cur)


def check_if_room_exists(uuid):
    conn, cur = get_db_assets_connection()
    try:
        cur.execute('SELECT id FROM rooms WHERE id = %s', (uuid,))
        return True if cur.fetchone() is not None else False
    finally:
        close_db_connection(conn, cur)


def check_if_building_has_existing_storey(uuid):
    conn, cur = get_db_assets_connection()
    try:
        cur.execute('SELECT id FROM storeys WHERE building_id = %s', (uuid,))
        return True if cur.fetchone() is not None else False
    finally:
        close_db_connection(conn, cur)


def check_if_storey_has_existing_rooms(uuid):
    conn, cur = get_db_assets_connection()
    try:
        cur.execute('SELECT id FROM rooms WHERE storey_id = %s', (uuid,))
        return True if cur.fetchone() is not None else False
    finally:
        close_db_connection(conn, cur)


def check_if_room_has_existing_reservations(uuid, tracing_id):
    reservation_url = 'http://' + os.environ.get('POSTGRES_RESERVATIONS_HOST') + '/api/' + \
                      os.environ.get('POSTGRES_RESERVATIONS_DBNAME') + '/?room_id=' + str(uuid)
    try:
        reservations = requests.get(reservation_url, headers={'uber-trace-id': tracing_id})
        if reservations.text == 'null':
            return False
        else:
            return True
    except:
        return True


def is_token_valid(token):
    if not token:
        return 'token is missing', False
    token = token.split(' ', 1)[1]
    key_url = 'http://' + os.environ.get('KEYCLOAK_HOST') + '/auth/realms/' + os.environ.get('KEYCLOAK_REALM')
    response = requests.get(key_url)
    public_key = '-----BEGIN PUBLIC KEY-----\n' + response.json()['public_key'] + '\n-----END PUBLIC KEY-----'
    public_key_encoded = bytes(public_key, 'UTF-8')
    try:
        jwt.decode(token, public_key_encoded, audience="account", algorithms=["RS256"])
        return 'token is valid', True
    except Exception as Error:
        return Error, False


def error_message(message):
    return jsonify({"message": str(message), "additionalProp1": {}})


# jwt
@app.route('/api/jwt/', methods=['GET'])
def returns_the_access_token():
    print(flask.request.json)
    return 'test', 200


# buildings
@app.route('/assets/buildings/', methods=['GET'])
def get_all_buildings():
    conn, cur = get_db_assets_connection()
    cur.execute('SELECT * FROM buildings;')
    buildings = cur.fetchall()
    close_db_connection(conn, cur)
    return jsonify(buildings), 200


@app.route('/assets/buildings/', methods=['POST'])
def add_a_new_building():
    conn, cur = get_db_assets_connection()
    data = flask.request.json
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    result = is_building(data)
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if result[1] is True:
            try:
                if check_if_building_exists(data['id']) is True:
                    cur.execute('UPDATE buildings SET name = (%s), address = (%s) WHERE id = (%s)',
                                (data['name'], data['address'], data['id']))
                    conn.commit()
                    return '', 200
                else:
                    cur.execute('INSERT INTO buildings (id, name, address) VALUES (%s, %s, %s)',
                                (data['id'], data['name'], data['address']))
                    conn.commit()
                    return '', 201
            except:
                conn.rollback()
                return error_message('DB error'), 500
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/buildings/<uuid>/', methods=['GET'])
def get_a_building_by_id(uuid):
    conn, cur = get_db_assets_connection()
    try:
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        cur.execute('SELECT * FROM buildings WHERE id = %s', (uuid,))
        building = cur.fetchone()
        if building is not None:
            return jsonify(building), 200
        else:
            return error_message('not found'), 404
    except:
        return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/buildings/<uuid>/', methods=['PUT'])
def add_or_update_a_building_by_id(uuid):
    conn, cur = get_db_assets_connection()
    data = flask.request.json
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    result = is_building(data)
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if result[1] is True and uuid == data['id']:
            try:
                if check_if_building_exists(data['id']) is True:
                    cur.execute('UPDATE buildings SET name = (%s), address = (%s) WHERE id = (%s)',
                                (data['name'], data['address'], data['id']))
                    conn.commit()
                    return '', 204
                else:
                    cur.execute('INSERT INTO buildings (id, name, address) VALUES (%s, %s, %s)',
                                (data['id'], data['name'], data['address']))
                    conn.commit()
                    return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
        elif uuid != data['id']:
            return error_message('mismatching id in url and object'), 422
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/buildings/<uuid>/', methods=['DELETE'])
def delete_a_building_by_id(uuid):
    conn, cur = get_db_assets_connection()
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        elif check_if_building_exists(uuid) is False:
            return error_message('not found'), 404
        else:
            if check_if_building_has_existing_storey(uuid) is True:
                return error_message('deletion not possible because of existing storeys'), 422
            try:
                cur.execute('DELETE FROM buildings WHERE id = %s;', (uuid,))
                conn.commit()
                return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


# storeys
@app.route('/assets/storeys/', methods=['GET'])
def get_all_storeys():
    uuid = flask.request.args.get('uuid')
    conn, cur = get_db_assets_connection()
    cur.execute('SELECT * FROM storeys WHERE building_id = %s;', (uuid,))
    storeys = cur.fetchall()
    close_db_connection(conn, cur)
    return jsonify(storeys), 200


@app.route('/assets/storeys/', methods=['POST'])
def add_a_new_storey():
    conn, cur = get_db_assets_connection()
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    data = flask.request.json
    result = is_storey(data)
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if result[1] is True:
            if check_if_building_exists(data['building_id']) is False:
                return error_message('building not found'), 422
            try:
                if check_if_storey_exists(data['id']) is True:
                    cur.execute('UPDATE storeys SET building_id = (%s), name = (%s) WHERE id = (%s)',
                                (data['building_id'], data['name'], data['id']))
                    conn.commit()
                    return '', 200
                else:
                    cur.execute('INSERT INTO storeys (id, building_id, name) VALUES (%s, %s, %s)',
                                (data['id'], data['building_id'], data['name']))
                    conn.commit()
                    return '', 201
            except:
                conn.rollback()
                return error_message('DB error'), 500
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/storeys/<uuid>/', methods=['GET'])
def get_a_storey_by_id(uuid):
    conn, cur = get_db_assets_connection()
    try:
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        cur.execute('SELECT * FROM storeys WHERE id = %s', (uuid,))
        storey = cur.fetchone()
        if storey is not None:
            return jsonify(storey), 200
        else:
            return error_message('not found'), 404
    except:
        return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/storeys/<uuid>/', methods=['PUT'])
def add_or_update_a_storeys_by_id(uuid):
    conn, cur = get_db_assets_connection()
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    data = flask.request.json
    result = is_storey(data)
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if uuid != data['id']:
            return error_message('mismatching id in url and object'), 422
        elif check_if_building_exists(data['building_id']) is False:
            return error_message('building not found'), 422
        elif result[1] is True and uuid == data['id']:
            try:
                if check_if_storey_exists(data['id']) is True:
                    cur.execute('UPDATE storeys SET building_id = (%s), name = (%s) WHERE id = (%s)',
                                (data['building_id'], data['name'], data['id']))
                    conn.commit()
                    return '', 204
                else:
                    cur.execute('INSERT INTO storeys (id, building_id, name) VALUES (%s, %s, %s)',
                                (data['id'], data['building_id'], data['name']))
                    conn.commit()
                    return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/storeys/<uuid>/', methods=['DELETE'])
def delete_a_storey_by_id(uuid):
    conn, cur = get_db_assets_connection()
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        elif check_if_storey_exists(uuid) is False:
            return error_message('not found'), 404
        else:
            if check_if_storey_has_existing_rooms(uuid) is True:
                return error_message('deletion not possible because of existing rooms'), 422
            try:
                cur.execute('DELETE FROM storeys WHERE id = %s;', (uuid,))
                conn.commit()
                return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


# rooms
@app.route('/assets/rooms/', methods=['GET'])
def get_all_rooms():
    conn, cur = get_db_assets_connection()
    cur.execute('SELECT * FROM rooms;')
    rooms = cur.fetchall()
    close_db_connection(conn, cur)
    return jsonify(rooms), 200


@app.route('/assets/rooms/', methods=['POST'])
def add_a_new_rooms():
    conn, cur = get_db_assets_connection()
    data = flask.request.json
    result = is_room(data)
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if result[1] is True:
            if check_if_storey_exists(data['storey_id']) is False:
                return error_message('storey not found'), 422
            try:
                if check_if_room_exists(data['id']) is True:
                    cur.execute('UPDATE rooms SET storey_id = (%s), name = (%s) WHERE id = (%s)',
                                (data['storey_id'], data['name'], data['id']))
                    conn.commit()
                    return '', 200
                else:
                    cur.execute('INSERT INTO rooms (id, storey_id, name) VALUES (%s, %s, %s)',
                                (data['id'], data['storey_id'], data['name']))
                    conn.commit()
                    return '', 201
            except:
                conn.rollback()
                return error_message('DB error'), 500
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/rooms/<uuid>/', methods=['GET'])
def get_a_room_by_id(uuid):
    conn, cur = get_db_assets_connection()
    try:
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        cur.execute('SELECT * FROM rooms WHERE id = %s', (uuid,))
        room = cur.fetchone()
        if room is not None:
            return jsonify(room), 200
        else:
            return error_message('not found'), 404
    except:
        return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/rooms/<uuid>/', methods=['PUT'])
def add_or_update_a_room_by_id(uuid):
    conn, cur = get_db_assets_connection()
    data = flask.request.json
    result = is_room(data)
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if uuid != data['id']:
            return error_message('mismatching id in url and object'), 422
        elif check_if_storey_exists(data['storey_id']) is False:
            return error_message('storey not found'), 422
        elif result[1] is True and uuid == data['id']:
            try:
                if check_if_room_exists(data['id']) is True:
                    cur.execute('UPDATE rooms SET storey_id = (%s), name = (%s) WHERE id = (%s)',
                                (data['storey_id'], data['name'], data['id']))
                    conn.commit()
                    return '', 204
                else:
                    cur.execute('INSERT INTO rooms (id, storey_id, name) VALUES (%s, %s, %s)',
                                (data['id'], data['storey_id'], data['name']))
                    conn.commit()
                    return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
        else:
            return error_message(result[0]), 400
    finally:
        close_db_connection(conn, cur)


@app.route('/assets/rooms/<uuid>/', methods=['DELETE'])
def delete_a_room_by_id(uuid):
    conn, cur = get_db_assets_connection()
    authentication = is_token_valid(flask.request.headers.get('Authorization'))
    tracing_id = flask.request.headers.get(os.environ.get('JAEGER_TRACECONTEXTHEADERNAME'))
    try:
        if authentication[1] is False:
            return error_message(authentication[0]), 401
        if is_valid_uuid(uuid) is False:
            return error_message('uuid not valid'), 404
        elif check_if_room_exists(uuid) is False:
            return error_message('not found'), 404
        else:
            if check_if_room_has_existing_reservations(uuid, tracing_id) is True:
                return error_message('deletion not possible because of existing reservations'), 422
            try:
                cur.execute('DELETE FROM rooms WHERE id = %s;', (uuid,))
                conn.commit()
                return '', 204
            except:
                conn.rollback()
                return error_message('DB error'), 500
    finally:
        close_db_connection(conn, cur)


if __name__ == '__main__':
    app.run(host=os.environ.get('FLASK_RUN_HOST'), port=5000)
